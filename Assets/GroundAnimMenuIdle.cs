﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundAnimMenuIdle : MonoBehaviour
{
    Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();

        GameStatesControl.instance.actionMenuIdle += GroundAnimEnable;
        GameStatesControl.instance.actionGameStart += GroundAnimDisable;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void GroundAnimEnable()
    {
        if(anim == null)
        {
            return;
        }
        anim.enabled = true;
    }
    public void GroundAnimDisable()
    {
        if (anim == null)
        {
            return;
        }
        anim.enabled = false;
    }
}
