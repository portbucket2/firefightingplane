﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathMaker : MonoBehaviour
{
    
    public PathStation[] pathStations;
    public List<PathPoint> pathPoints;
    public GameObject pathPointsHolder;

    //public static PathMaker instance;

    private void Awake()
    {
        //instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        InitialTasksPathMaker();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ConnectPathStations()
    {
        for (int i = 0; i < pathStations.Length; i++)
        {
            if(i == 0)
            {
                pathStations[i].previousPathStation = null;
                pathStations[i].nextPathStation = pathStations[i + 1];
            }
            else if(i == pathStations.Length - 1)
            {
                pathStations[i].previousPathStation = pathStations[i - 1];
                pathStations[i].nextPathStation = null;
            }
            else
            {
                pathStations[i].previousPathStation = pathStations[i - 1];
                pathStations[i].nextPathStation = pathStations[i +1];
            }
        }
    }
    public void CreateIntermediatePoints()
    {
        for (int i = 0; i < pathStations.Length; i++)
        {
            pathStations[i].CreatPathPoints(this);
        }
    }
    public void AssignNeighbourToPathPoints()
    {
        for (int i = 0; i < pathPoints.Count; i++)
        {
            pathPoints[i].transform.SetParent(pathPointsHolder.transform);
            if (i == 0)
            {
                pathPoints[i].previousPathPoint = null;
                pathPoints[i].nextPathPoint = pathPoints[i + 1];
            }
            else if (i == pathPoints.Count - 1)
            {
                pathPoints[i].previousPathPoint = pathPoints[i - 1];
                pathPoints[i].nextPathPoint = null;
            }
            else
            {
                pathPoints[i].previousPathPoint = pathPoints[i - 1];
                pathPoints[i].nextPathPoint = pathPoints[i + 1];
            }
        }
    }
    public void InitialTasksPathMaker()
    {
        //pathStations = LevelValuesAssignManager.instance.pathOnLevel[GameManagementMain.instance.levelIndex].pathStations;
        ConnectPathStations();
        CreateIntermediatePoints();
        AssignNeighbourToPathPoints();
        //PlayerPathFollowSystem.instance.PathFollowAssignTarget(pathPoints[0]);
    }
    public void Reset()
    {
        
    }
}
