﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    public GameObject tutInitialHolder;
    public GameObject FlyOverFireText;
    public int tutDone;

    public static TutorialManager instance;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        

        GameStatesControl.instance.actionMenuIdle += FlyOverFireTextDisable;
        GameStatesControl.instance.actionMenuIdle += TutorialHandActivate;
    }

    // Update is called once per frame
    //void Update()
    //{
    //    
    //}
    public void SwipeActionFunTut()
    {
        
        if (GameStatesControl.instance.GetCurrentGameState() == GameStates.MenuIdle)
        {
            UiManage.instance.StartButtonFun();
            if (tutDone > 0)
            {
                TutorialHandDEactivate();
            }
            else
            {
                Invoke("TutorialHandDEactivate", 5f);
                tutDone += 1;
            }
            DeAssignSwipeActions();
        }
        
    }
    public void AssignSwipeActions()
    {
        InputMouseSystem.instance.actionSwipeRight += SwipeActionFunTut;
        InputMouseSystem.instance.actionSwipeLeft += SwipeActionFunTut;
    }
    public void DeAssignSwipeActions()
    {
        InputMouseSystem.instance.actionSwipeRight -= SwipeActionFunTut;
        InputMouseSystem.instance.actionSwipeLeft -= SwipeActionFunTut;
    }
    public void TutorialHandDEactivate()
    {
        tutInitialHolder.SetActive(false);
    }
    public void TutorialHandActivate()
    {
        tutInitialHolder.SetActive(true);
        AssignSwipeActions();
    }
    public void FlyOverFireTextEnable()
    {
        FlyOverFireText.SetActive(true);
    }
    public void FlyOverFireTextDisable()
    {
        FlyOverFireText.SetActive(false);
    }
}
