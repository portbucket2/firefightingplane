﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagementMain : MonoBehaviour
{
    [SerializeField]
    public bool usePlayerPrefs;
    public int levelIndexDisplayed;
    public int levelIndex;
    [SerializeField]
    int maxLevelIndex = 4;
    public static GameManagementMain instance;

    private void Awake()
    {
        instance = this;
        if (usePlayerPrefs)
        {
            levelIndexDisplayed = PlayerPrefs.GetInt("levelIndexDisplayed", 0);
        }
        
        CalculateLevelIndex();

    }
    // Start is called before the first frame update
    void Start()
    {
#if UNITY_EDITOR
        Debug.unityLogger.logEnabled = true;
#else
             Debug.unityLogger.logEnabled=false;
#endif
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void NextLevelUpdate()
    {
        levelIndexDisplayed += 1;
        PlayerPrefs.SetInt("levelIndexDisplayed", levelIndexDisplayed);
        CalculateLevelIndex();
    }
    void CalculateLevelIndex()
    {
        levelIndex = (levelIndexDisplayed ) % (maxLevelIndex + 1);
    }

    public void AnalyticsCallLevelStarted()
    {
        int level = levelIndexDisplayed + 1;
        //Debug.Log("LevelStarted " + level);
        LionStudios.Analytics.Events.LevelStarted(level, 0);
    }
    public void AnalyticsCallLevelAccomplished()
    {
        int level = levelIndexDisplayed + 1;
        //Debug.Log("LevelAccomplished " + level);
        LionStudios.Analytics.Events.LevelComplete(level, 0);
    }
    public void AnalyticsCallLevelFailed()
    {
        int level = levelIndexDisplayed + 1;
        //Debug.Log("LevelFailed " + level);
        LionStudios.Analytics.Events.LevelFailed(level, 0);
    }
}
