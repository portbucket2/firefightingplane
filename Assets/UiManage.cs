﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManage : MonoBehaviour
{
    public GameObject panelMenuIdle;
    public GameObject panelLevelResult;
    public GameObject panelLevelEnd;
    public GameObject panelGamePlay;

    public GameObject levelResultSuccess;
    public GameObject levelResultFail;
    public GameObject levelEndSuccess;
    public GameObject levelEndFail;

    public static UiManage instance;
    public Text levelText;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    //void Update()
    //{
    //    
    //}

    public void MenuIdlePanelAppear()
    {
        panelMenuIdle    .SetActive(true);
        panelLevelResult .SetActive(false);
        panelLevelEnd    .SetActive(false);
        panelGamePlay    .SetActive(false);
    }
    public void GamePlayPanelAppear()
    {
        panelMenuIdle.SetActive(false);
        panelLevelResult.SetActive(false);
        panelLevelEnd.SetActive(false);
        panelGamePlay.SetActive(true);
    }
    public void LevelResultPanelAppear()
    {
        panelMenuIdle.SetActive(false);
        panelLevelResult.SetActive(true);
        panelLevelEnd.SetActive(false);
        panelGamePlay.SetActive(false);
    }
    public void LevelEndPanelAppear()
    {
        panelMenuIdle.SetActive(false);
        panelLevelResult.SetActive(false);
        panelLevelEnd.SetActive(true);
        panelGamePlay.SetActive(false);
    }

    public void UiSuccessPanelsAppear()
    {
        levelResultSuccess.SetActive(true);
        levelResultFail   .SetActive(false);
        levelEndSuccess   .SetActive(true);
        levelEndFail      .SetActive(false);
    }
    public void UiFailedPanelsAppear()
    {
        levelResultSuccess.SetActive(false);
        levelResultFail.SetActive(true);
        levelEndSuccess.SetActive(false);
        levelEndFail.SetActive(true);
    }
    public void StartButtonFun()
    {
        
        GameStatesControl.instance.ChangeGameStateTo(GameStates.GameStart);
    }
    public void RestartButtonFun()
    {
        //Application.LoadLevel(0);
        LevelManager.instance.LevelReset();
        GameStatesControl.instance.ChangeGameStateTo(GameStates.MenuIdle);
    }
    public void NextButtonFun()
    {
        GameManagementMain.instance.NextLevelUpdate();
        RestartButtonFun();
        //GameManagementMain.instance.NextLevelUpdate();
    }
    public void UiValuesUpdate()
    {
        levelText.text = "LEVEL " + (GameManagementMain.instance.levelIndexDisplayed+1);
    }
}
