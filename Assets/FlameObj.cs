﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlameObj : MonoBehaviour
{
    public Slider flameSlider;
    public float flameLevel;
    public float flameLevelMax;
    public bool rescued;
    public ParticleSystem fireParticle;
    public ParticleSystem smokeParticle;
    float fireEmission;
    float smokeEmission;
    public float fireReduceRate;
    // Start is called before the first frame update
    void Start()
    {
        fireEmission = fireParticle.emissionRate;
        smokeEmission =  smokeParticle.emissionRate;
        flameSlider.value = flameLevel; 
    }

    // Update is called once per frame
    //void Update()
    //{
    //    
    //}
    //
    public void FalmeReduce()
    {
        float rate = fireReduceRate;
        if (rescued)
        {
            return;
        }
        if (flameLevel <= 0)
        {
            KillFlame();
            return;
        }
        flameLevel -= rate*Time.deltaTime*100f;
        flameSlider.value = flameLevel;

        fireParticle.emissionRate = fireEmission * flameLevel * 0.01f;
        smokeParticle.emissionRate = smokeEmission  *flameLevel * 0.01f;

    }
    public void KillFlame()
    {
        rescued = true;
        flameSlider.gameObject.SetActive(false);
        LevelManager.instance.firefightingManagerInThisLevel.flameObjsInCurrent.Remove(this);
        fireParticle.Stop();
        smokeParticle.Stop();
        fireParticle .Clear();
        smokeParticle.Clear();
        if (LevelManager.instance.firefightingManagerInThisLevel.flameObjsInCurrent.Count <= 0)
        {
            LevelManager.instance.firefightingManagerInThisLevel.FirefightingSuccessful();
        }
    }

    public void ResetIt()
    {
        rescued = false;
        flameSlider.gameObject.SetActive(true);
        flameLevel = flameLevelMax;
        flameSlider.value = flameLevel;
        fireParticle.emissionRate = fireEmission * flameLevel * 0.01f;
        smokeParticle.emissionRate = smokeEmission * flameLevel * 0.01f;
    }
}
