﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRightLeftMovement : MonoBehaviour
{
    public GameObject playerChild;
    public Vector3 rot;
    public float maxRightLeft;
    Vector3 oldPos;
    Vector3 newPos;
    float sideVel;
    Vector3 smoothOldPos;
    public bool centerSnapMode;
    float centerSnapSpeed;
    // Start is called before the first frame update
    void Start()
    {
        InputMouseSystem.instance.actionTouchIn += GetOldPos;
        InputMouseSystem.instance.actionTouchHolded += NewPosUpdate;
        centerSnapSpeed = GetComponent<PlayerPathFollowSystem>().moveSpeed / 3f;
    }

    // Update is called once per frame
    void Update()
    {
        //RotateChildAsDirected();

        
        DragFollow();
    }
    public void RotateChildAsDirected()
    {
         rot.y = Input.GetAxis("Horizontal") * 35 ;
        rot.z = rot.y * 0.25f;
        playerChild.transform.localRotation = Quaternion.Euler(rot);
        playerChild. transform.localPosition += new Vector3(rot.y * 0.01f, 0, 0);
        playerChild.transform.localPosition = new Vector3(Mathf.Clamp(playerChild.transform.localPosition.x, -maxRightLeft, maxRightLeft), 0, 0);

    }

    public void GetOldPos()
    {
        oldPos = playerChild.transform.localPosition;
        if (centerSnapMode)
        {
            return;
        }
        newPos = oldPos;
        //smoothOldPos = oldPos;
    }
    public void NewPosUpdate()
    {
        if (centerSnapMode)
        {
            return;
        }
        //newPos = oldPos + new Vector3(InputMouseSystem.instance.mouseProgression.x  *0.05f, 0, 0);
        newPos += new Vector3(InputMouseSystem.instance.mouseVelocity.x*884f * 0.08f, 0, 0);
        newPos = new Vector3(Mathf.Clamp(newPos.x, -maxRightLeft, maxRightLeft), 0, 0);

    }
    public void DragFollow()
    {
        if (centerSnapMode)
        {
            ReturnToCenter();
        }
        playerChild.transform.localPosition = Vector3.Lerp(playerChild.transform.localPosition, newPos, Time.deltaTime * 7);
        //playerChild.transform.localPosition = new Vector3(Mathf.Clamp(playerChild.transform.localPosition.x, -maxRightLeft, maxRightLeft), 0, 0);
        sideVel = (playerChild.transform.localPosition - smoothOldPos).x / Time.deltaTime;
        smoothOldPos = playerChild.transform.localPosition;

        rot.y =Mathf.Clamp(  sideVel * 0.025f * 35 , -35,35);
        rot.z = -rot.y * 0.25f;
        playerChild.transform.localRotation = Quaternion.Lerp(playerChild.transform.localRotation, Quaternion.Euler(rot) , Time.deltaTime*10);


    }
    public void ReturnToCenter()
    {
        newPos = Vector3.MoveTowards(newPos, Vector3.zero, Time.deltaTime * 10 * centerSnapSpeed);
    }
    public void EnableCenterSnapMode()
    {
        centerSnapMode = true;
    }
    public void DisableCenterSnapMode()
    {
        centerSnapMode = false;
    }
    public void ResetIt()
    {
        EnableCenterSnapMode();
        newPos = Vector3.zero;
        playerChild.transform.localPosition = Vector3.zero;
        playerChild.transform.localRotation = Quaternion.Euler(Vector3.zero);
    }

}
