﻿public enum PlayerRunMode
{
    Stationary,PathFollowing,DragControlled
}
public enum WaterTankMode
{
    Stationary,CollectWater,SprayWater
}
public enum CameraMode
{
    TopDown, BackClose, Idle
}
public enum GameStates
{
    MenuIdle   ,
    GameStart  ,
    LevelStart ,
    GamePlay   ,
    LevelResult,
    LevelEnd   
}
public enum LevelResultType
{
    Success, Fail
}
