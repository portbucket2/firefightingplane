﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerWaterTank : MonoBehaviour
{
    //public Slider waterTankSlider;
    public float waterLevel;
    public float waterLevelMax;
    public bool collectWater;
    public bool sprayWater;
    public LayerMask flameLayer;
    public LayerMask waterLayer;
    public WaterTankMode waterTankMode;
    public ParticleSystem waterSprayParticle;
    bool isDead;
    public static PlayerWaterTank instance;
    FirefightingManager firefightingManager;
    public float waterfallRange;
    public bool hitWater;
    public ParticleSystem waterSplashParticle;
    public Image waterTankImage;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        TankSliderUpdate();
        ChangeWaterTankModeTo(WaterTankMode.Stationary);
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isDead)
        {
            return;
        }
        if (collectWater)
        {
            FillWater();
        }
        if (sprayWater)
        {
            //RaycastForFlame();
            if(GameStatesControl.instance.GetCurrentGameState() == GameStates.GamePlay)
            {
                SpendWater(2);
            }
            
        }
        RaycastForWaterbed();


    }
    public void ChangeWaterTankModeTo(WaterTankMode mode)
    {
        waterTankMode = mode;
        switch (waterTankMode)
        {
            case WaterTankMode.Stationary:
                {
                    collectWater = false;
                    sprayWater = false;
                    break;
                }
            case WaterTankMode.CollectWater:
                {
                    collectWater = true;
                    sprayWater = false;
                    break;
                }
            case WaterTankMode.SprayWater:
                {
                    collectWater = false;
                    sprayWater = true;
                    waterSprayParticle.Play();
                    break;
                }
        }
    }

    public void FillWater()
    {
        if(waterLevel >= waterLevelMax)
        {
            return;
        }
        waterLevel += Time.deltaTime*12;
        TankSliderUpdate();
    }
    public void SpendWater(float speed)
    {
        if (waterLevel <= 0)
        {
            StopWaterFall();

            LevelManager.instance.SetLevelResultTo(LevelResultType.Fail);

            GameStatesControl.instance.ChangeGameStateTo(GameStates.LevelResult, 2);
            return;
        }
        //RaycastForFlame();
        Firefight();
        waterLevel -= Time.deltaTime * speed;
        TankSliderUpdate();
    }
    public void TankSliderUpdate()
    {
        //waterTankSlider.value = waterLevel;
        waterTankImage.fillAmount = waterLevel * 0.01f;
    }
    public void StopWaterFall()
    {
        waterSprayParticle.Stop();
        ChangeWaterTankModeTo(WaterTankMode.Stationary);
    }

    public void RaycastForFlame()
    {

        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(transform.position, -Vector3.up, out hit, Mathf.Infinity,flameLayer))
        {
            //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            //Debug.Log("Did Hit");
            hit.transform.gameObject.GetComponent<FlameObj>().FalmeReduce();
        }
        else
        {
            //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
            //Debug.Log("Did not Hit");
        }
    }
    public void RaycastForWaterbed()
    {

        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(transform.position, -Vector3.up, out hit, 5f, waterLayer))
        {
            //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            //Debug.Log("Did Hit");
            if (!hitWater)
            {
                hitWater = true;
                waterSplashParticle.Play();
            }
            //hit.transform.gameObject.GetComponent<FlameObj>().FalmeReduce();
        }
        else
        {
            if (hitWater)
            {
                hitWater = false;
                Invoke("StopWaterSplash", 0.25f);
            }
            //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
            //Debug.Log("Did not Hit");
        }
    }
    public void StopWaterSplash()
    {
        waterSplashParticle.Stop();
    }
    public void Firefight()
    {
        for (int i = 0; i < firefightingManager.flameObjsInCurrent.Count; i++)
        {
            Vector3 v = firefightingManager.flameObjsInCurrent[i].transform.position;
            v.y = transform.position.y;
            if(Vector3.Distance (transform.position , v) < waterfallRange)
            {
                firefightingManager.flameObjsInCurrent[i].gameObject.GetComponent<FlameObj>().FalmeReduce();
            }
            if (firefightingManager.flameObjsInCurrent.Count >= (i + 1))
            {
                if ((PlayerPathFollowSystem.instance.transform.position.z - firefightingManager.flameObjsInCurrent[i].gameObject.transform.position.z) > 20f)
                {


                    CamFollow.instance.FocusOnObj(firefightingManager.flameObjsInCurrent[i].transform.gameObject);
                    StopWaterFall();
                    LevelManager.instance.SetLevelResultTo(LevelResultType.Fail);
                    GameStatesControl.instance.ChangeGameStateTo(GameStates.LevelResult);
                    PlayerPathFollowSystem.instance.BrakeMultiplierTo(0, 0.25f);


                }
            }
            
        }
    }
    public void KillWaterTank()
    {
        //waterTankSlider.gameObject.SetActive(false);
        isDead = true;
    }
    public void ResetIt()
    {
        isDead = false;
        //waterTankSlider.gameObject.SetActive(true);
        waterLevel = 0;
        TankSliderUpdate();
        ChangeWaterTankModeTo(WaterTankMode.Stationary);
        StopWaterSplash();
        hitWater = false;
    }
    public void InitialTasks()
    {
        firefightingManager = LevelManager.instance.firefightingManagerInThisLevel;
    }
}
