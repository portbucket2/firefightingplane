﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public LevelResultType levelResultType;
    public static LevelManager instance;
    public FirefightingManager firefightingManagerInThisLevel;
    public GameObject[] levelParents;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetLevelResultTo(LevelResultType l)
    {
        levelResultType = l;
        switch (levelResultType)
        {
            case LevelResultType.Success:
                {
                    UiManage.instance.UiSuccessPanelsAppear();
                    GameManagementMain.instance.AnalyticsCallLevelAccomplished();
                    break;
                }
            case LevelResultType.Fail:
                {
                    UiManage.instance.UiFailedPanelsAppear();
                    GameManagementMain.instance.AnalyticsCallLevelFailed();
                    break;
                }
        }
    }
    public void MenuIdleFun()
    {
        //for (int i = 0; i < levelParents.Length; i++)
        //{
        //    levelParents[i].SetActive(false);
        //}
        MakeLevelAlive();
    }
    public void GameStartFun()
    {
        firefightingManagerInThisLevel= LevelValuesAssignManager.instance.firefightinngAreaInLevels[GameManagementMain.instance.levelIndex];
    }
    public void LevelStartFun()
    {
        //MakeLevelAlive();
        PlayerControlSystemDefiner.instance.InitialTasksPlayer();
        //PathMaker.instance.InitialTasksPathMaker();
        
        CamFollow.instance.ChangeCameraModeTo(CameraMode.BackClose);
    }
    public void LevelReset()
    {
        firefightingManagerInThisLevel.ResetFirefightingManager();
        PlayerControlSystemDefiner.instance.ResetPlayer();
        CamFollow.instance.ResetIt();
    }

    public void MakeLevelAlive()
    {
        for (int i = 0; i < levelParents.Length; i++)
        {
            if(i == GameManagementMain.instance.levelIndex)
            {
                levelParents[i].SetActive(true);
            }
            else
            {
                levelParents[i].SetActive(false);
            }
        }
    }
}
