﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    Rigidbody rb;
    public GameObject deadPlaneHolder;
    public bool isDead;
    public BoxCollider[] colliders;
    public ParticleSystem planeFireParticle;
    Vector3 localPosPlayerMesh;
    public static PlayerHealth instance;
    public TrailRenderer[] wingTrails;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        localPosPlayerMesh = transform.localPosition;
    }

    // Update is called once per frame
    //void Update()
    //{
    //    //if (Input.GetKeyDown(KeyCode.R))
    //    //{
    //    //    ResetIt();
    //    //}
    //}

    public void KillPlane(GameObject obs)
    {
        rb.isKinematic = false;
        rb.useGravity = true;
        transform.SetParent(deadPlaneHolder.transform);
        for (int i = 0; i < colliders.Length; i++)
        {
            colliders[i].isTrigger = false;
        }
        
        float s =Mathf.Clamp( transform.InverseTransformPoint(obs.transform.position).x , -5 , 5);
        rb.AddForce(-s *100, 800, 500);
        rb.AddTorque(Random.Range(-1000f, 1000f),s* 800, Random.Range(-1000f, 1000f));
        isDead = true;
        PlayerControlSystemDefiner.instance.ChangePlayerRunModeTo(PlayerRunMode.Stationary);
        GetComponent<PlayerWaterTank>().KillWaterTank();
        planeFireParticle.Play();

        LevelManager.instance.SetLevelResultTo(LevelResultType.Fail);

        GameStatesControl.instance.ChangeGameStateTo(GameStates.LevelResult, 2);


    }

    private void OnTriggerEnter(Collider other)
    {
        if (isDead)
        {
            return;
        }
        if(other.gameObject.layer == 8)
        {
            if (!isDead)
            {
                KillPlane(other.gameObject);
            }
        }
    }
    public void ResetIt()
    {
        isDead = false;
        planeFireParticle.Stop();
        planeFireParticle.Clear();
        rb.isKinematic = true;
        rb.useGravity = false;
        transform.SetParent(PlayerPathFollowSystem.instance.GetComponent<PlayerRightLeftMovement>().playerChild.transform);
        transform.localPosition = localPosPlayerMesh;
        transform.localRotation = Quaternion.Euler(Vector3.zero);
        for (int i = 0; i < colliders.Length; i++)
        {
            colliders[i].isTrigger = true;
        }

        wingTrails[0].Clear();
        wingTrails[1].Clear();
    }
}
