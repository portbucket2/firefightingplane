﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPathFollowSystem : MonoBehaviour
{
    public PathPoint target;
    public float moveSpeed;
    float moveSpeedBrakeMul = 1;
    public static PlayerPathFollowSystem instance;
    public Vector3 sideOffsetOfPathpoint;
    bool pathFollowEnabled;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!pathFollowEnabled)
        {
            return;
        }
        PathFollow();
        //sideOffsetOfPathpoint.x = Input.GetAxis("Horizontal") * 5;
    }
    public void PathFollow()
    {
        if(target == null)
        {
            return;
        }
        transform.position = Vector3.MoveTowards(transform.position, target.transform.position + sideOffsetOfPathpoint , Time.deltaTime * 10 * moveSpeed * moveSpeedBrakeMul);
        
        if(Vector3.Distance ( transform.position ,(target.transform.position + sideOffsetOfPathpoint)) <5f)
        {
            if (target.nextPathPoint != null)
            {
                target = target.nextPathPoint;
            }
            else
            {
                target = null;
            }
            
        }
        if (target == null)
        {
            return;
        }
        transform.rotation =Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(-transform.position + (target.transform.position + sideOffsetOfPathpoint), Vector3.up),Time.deltaTime*5);
    }
    public void EnablePathFollowMode()
    {
        pathFollowEnabled = true;
        BrakeMultiplierTo(1, 1,true);
    }
    public void DisablePathFollowMode()
    {
        pathFollowEnabled = false;
    }
    public void PathFollowAssignTarget(PathPoint p)
    {
        target = p;
    }

    public IEnumerator BrakeMulTurnInto(float v , float t = 1)
    {
        yield return new WaitForSeconds(0);
        if(moveSpeedBrakeMul != v)
        {
            moveSpeedBrakeMul = Mathf.MoveTowards(moveSpeedBrakeMul, v, Time.deltaTime * t*(moveSpeed/3f));
            StartCoroutine(BrakeMulTurnInto(v, t));
        }
    }
    public void ResetIt()
    {
        DisablePathFollowMode();
    }
    public void BrakeMultiplierTo(float v, float t = 1,bool insatnat = false)
    {
        if (insatnat)
        {
            moveSpeedBrakeMul = v;
        }
        else
        {
            StartCoroutine(BrakeMulTurnInto(v, t));
        }
        
    }
}
