﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathStation : MonoBehaviour
{
    public PathStation previousPathStation;
    public PathStation nextPathStation;
    public int subdivisions;
    public PathPoint pathPointPrefab;
    public GameObject frontHandle;
    public GameObject backHandle;
    public List<PathPoint> pathPointsCreated;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CreatPathPoints(PathMaker pathMaker)
    {
        if(subdivisions == 0)
        {
            return;
        }
        if(nextPathStation == null)
        {
            return;
        }
        float increment = 1f / (float)subdivisions;

        for (int i = 0; i < subdivisions; i++)
        {
            Vector3 pos1 = Vector3.Lerp(transform.position, frontHandle.transform.position, i * increment);
            Vector3 pos3= Vector3.Lerp(nextPathStation.backHandle.transform.position, nextPathStation.transform.position, i * increment);
            Vector3 pos2 = Vector3.Lerp(frontHandle.transform.position,nextPathStation.backHandle.transform.position, i * increment);
            Vector3 pos4 = Vector3.Lerp(pos1,pos2, i * increment);
            Vector3 pos5 = Vector3.Lerp(pos2,pos3, i * increment);
            Vector3 pos6 = Vector3.Lerp(pos4, pos5, i * increment);
            GameObject go = Instantiate(pathPointPrefab.gameObject, pos6, Quaternion.identity);

            pathMaker.pathPoints.Add(go.GetComponent<PathPoint>());
            pathPointsCreated.Add(go.GetComponent<PathPoint>());
        }
    }
}
