﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFollow : MonoBehaviour
{
    public GameObject playerObj;
    public GameObject target;
    public GameObject camParent;
    Transform camTargetTrans;
    public Transform topDownTrans;
    public Transform backCloseTrans;
    public Transform idleCloseTrans;
    public float[] camFovs;
    public CameraMode cameraMode;
    public static CamFollow instance;
    Camera camMain;
    AnimationCurve animC = AnimationCurve.EaseInOut(0f,0f,1f,1f);
    bool emergencyFocus;
    //public AnimationCurve animFov ;
    private void Awake()
    {
        instance = this;

    }
    // Start is called before the first frame update
    void Start()
    {
        //camTargetTrans = backCloseTrans;
        //ChangeCameraModeTo(CameraMode.Idle);
        camMain = Camera.main;
        FocusOnObj(playerObj) ;
    }

    // Update is called once per frame
    void Update()
    {
       if(target != null)
        {
            transform.position = Vector3.Lerp(transform.position, target.transform.position, Time.deltaTime * 5);
            Vector3 r = Quaternion.ToEulerAngles(target.transform.rotation) * Mathf.Rad2Deg;
            Quaternion q = Quaternion.Euler(new Vector3(r.x*0.5f, 0, 0));
            transform.rotation = Quaternion.Lerp(transform.rotation, q, Time.deltaTime * 5);

        } 
       if(camTargetTrans == null)
        {
            return;
        }
       //if(camParent.transform.position != camTargetTrans.position)
       //{
       //    camParent.transform.position =Vector3.Lerp(camParent.transform.position, Vector3.MoveTowards(camParent.transform.position, camTargetTrans.position, Time.deltaTime * 100), Time.deltaTime * 5);
       //     //camParent.transform.rotation = Quaternion.RotateTowards(camParent.transform.rotation, camTargetTrans.rotation, Time.deltaTime *20);
       // }
       // if (camParent.transform.rotation != camTargetTrans.rotation)
       // {
       //     ///camParent.transform.position = Vector3.MoveTowards(camParent.transform.position, camTargetTrans.position, Time.deltaTime * 20);
       //     //camParent.transform.rotation =Quaternion.Lerp(camParent.transform.rotation, Quaternion.RotateTowards(camParent.transform.rotation, camTargetTrans.rotation, Time.deltaTime * 100),Time.deltaTime*20);
       //     camParent.transform.rotation = Quaternion.Lerp(camParent.transform.rotation, camTargetTrans.rotation, Time.deltaTime * 2);
       // }
    }

    public void ChangeCameraModeTo(CameraMode c)
    {
        cameraMode = c;
        switch (cameraMode)
        {
            case CameraMode.TopDown:
                {
                    camTargetTrans = topDownTrans;
                    CamTransition(camTargetTrans, camFovs[2]);
                    break;
                }
            case CameraMode.BackClose:
                {
                    camTargetTrans = backCloseTrans;
                    CamTransition(camTargetTrans, camFovs[1]);
                    break;
                }
            case CameraMode.Idle:
                {
                    camTargetTrans = idleCloseTrans;
                    CamTransition(camTargetTrans, camFovs[0]);
                    break;
                }
        }
    }

    public IEnumerator CamTransitionCoroutine(float progression,float speed, Vector3 fromP, Vector3 toP , Quaternion fromR , Quaternion toR, float fromF, float toF)
    {
        yield return new WaitForSeconds(0);
        if(progression < 1)
        {
            progression += Time.deltaTime * speed;
            camParent.transform.localPosition = Vector3.Lerp(fromP, toP, animC.Evaluate(progression));
            camParent.transform.localRotation = Quaternion.Lerp(fromR, toR, animC.Evaluate(progression));
            camMain.fieldOfView = Mathf.Lerp(fromF, toF, animC.Evaluate(progression));
            StartCoroutine(CamTransitionCoroutine(progression, speed, fromP, toP, fromR, toR, fromF, toF));
        }
        else
        {
            progression = 1;
            camParent.transform.localPosition = Vector3.Lerp(fromP, toP, animC.Evaluate(progression));
            camParent.transform.localRotation = Quaternion.Lerp(fromR, toR, animC.Evaluate(progression));
            camMain.fieldOfView = Mathf.Lerp(fromF, toF, animC.Evaluate(progression));
        }
    }
    public void CamTransition(Transform to, float fov, bool instant = false)
    {
        Vector3 v1 = camParent.transform.localPosition;
        Vector3 v2 = to.localPosition;
        Quaternion r1 = camParent.transform.localRotation;
        Quaternion r2 = to.localRotation;
        float f1 = camMain.fieldOfView;
        float f2 = fov;
        
        if (instant)
        {
            StartCoroutine(CamTransitionCoroutine(1f, 0.5f, v1, v2, r1, r2, f1, f2));
        }
        else
        {
            StartCoroutine(CamTransitionCoroutine(0f, 0.5f, v1, v2, r1, r2, f1, f2));
        }
    }

    public void ResetIt()
    {
        FocusOnObj(playerObj);
        transform.position = target.transform.position;
        camParent.transform.localPosition = idleCloseTrans.position;
        camParent.transform.localRotation = idleCloseTrans.rotation;
        ChangeCameraModeTo(CameraMode.Idle);
        StopAllCoroutines();
        CamTransition(camTargetTrans, camFovs[0],true);

        
    }
    public void InitialTasks()
    {

    }
    public void FocusOnObj(GameObject go)
    {
        target = go;
    }
}
