﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDragMoveSystem : MonoBehaviour
{
    public Vector3 tarPos;
    public Vector3 lastPos;
    bool dragModeEnabled;
    public JoystickController joystickController;
    public bool joysticUse;
    public bool leftRightFirefight;
    float moveSpeed;
    public float a;
    public float b;
    public float c;
    FirefightingManager firefightingManager;
    //Quaternion oldRot;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!dragModeEnabled)
        {
            return;
        }
        if (joysticUse)
        {
            JoystickMove();
            //JoystickMoveLikeCar();
        }
        else
        {
            DragMove();
        }
        //
        
    }

    public void DragMove()
    {
        float sensitivity = 0.15f;
        tarPos += new Vector3(InputMouseSystem.instance.mouseVelocity.x * 884f * sensitivity, 0, InputMouseSystem.instance.mouseVelocity.y * 884f * sensitivity);
        transform.position = Vector3.Lerp(transform.position,tarPos, Time.deltaTime * 6);
        if(Vector3.Distance(transform.position, tarPos) > 1f)
        {
            Quaternion tarRot = Quaternion.LookRotation(-transform.position + tarPos, Vector3.up);
            Quaternion finalRot = Quaternion.RotateTowards(transform.rotation, tarRot, Time.deltaTime * 1200);
            transform.rotation = Quaternion.Lerp(transform.rotation, tarRot, Time.deltaTime *20);
        }
        
        lastPos = transform.position;
    }
    public void JoystickMove()
    {
        float sensitivity = c;
        tarPos += new Vector3(joystickController.output.x * sensitivity, 0, joystickController.output.y* sensitivity);
        tarPos.x = Mathf.Clamp(tarPos.x, firefightingManager.MinX.transform.position.x, firefightingManager.MaxX.transform.position.x);
        tarPos.z = Mathf.Clamp(tarPos.z, firefightingManager.MinZ.transform.position.z, firefightingManager.MaxZ.transform.position.z);
        transform.position = Vector3.Lerp(transform.position, tarPos, Time.deltaTime * 6);
        if (Vector3.Distance(transform.position, tarPos) > 0.5f)
        {
            Quaternion tarRot = Quaternion.LookRotation(-transform.position + tarPos, Vector3.up);
            Quaternion finalRot = Quaternion.RotateTowards(transform.rotation, tarRot, Time.deltaTime * a);
            transform.rotation = Quaternion.Lerp(transform.rotation, finalRot, Time.deltaTime * b);
        }

        lastPos = transform.position;
    }
    public void JoystickMoveLikeCar()
    {
        float sensitivity = .5f;
        //float ang = Mathf.Atan2(joystickController.output.x , joystickController.output.y);
        //tarPos += new Vector3(joystickController.output.x * sensitivity, 0, joystickController.output.y * sensitivity);
        //transform.position = Vector3.Lerp(transform.position, tarPos, Time.deltaTime * 6);
        //if (Vector3.Distance(transform.position, tarPos) > 0.5f)
        //{
        //    Quaternion tarRot = Quaternion.LookRotation(-transform.position + tarPos, Vector3.up);
        //    Quaternion finalRot = Quaternion.RotateTowards(transform.rotation, tarRot, Time.deltaTime * 1200);
        //    transform.rotation = Quaternion.Lerp(transform.rotation, tarRot, Time.deltaTime * 20);
        //}
        //
        //lastPos = transform.position;
        
        if (moveSpeed < 0.1f)
        {
            //return;
        }
        Quaternion tarRot = Quaternion.Euler(new Vector3(0, joystickController.ang, 0));
        Quaternion finalRot = Quaternion.RotateTowards(transform.rotation, tarRot, Time.deltaTime * a);
        Quaternion oldRot = transform.rotation;
        transform.rotation = Quaternion.Lerp(transform.rotation, finalRot, Time.deltaTime * b);

        float rotSpeed = ((Quaternion.ToEulerAngles(transform.rotation) - Quaternion.ToEulerAngles(oldRot)).y * Mathf.Rad2Deg )/ Time.deltaTime;
        
        //Vector3 r = Quaternion.ToEulerAngles(finalRot) * Mathf.Rad2Deg;
        //float f = 1f - Mathf.Clamp((Mathf.Abs(r.y - joystickController.ang) / 180), 0f, 0.5f);
        float f = 1f - Mathf.Clamp((rotSpeed * 0.01f), 0f, 1f);
        //Debug.Log(rotSpeed * 0.01f);
        float m = joystickController.output.magnitude;
        moveSpeed = Mathf.Lerp(moveSpeed, m, Time.deltaTime * 2);
        transform.Translate(0, 0, moveSpeed *f*c* 50 * Time.deltaTime);
        //float speed = 


    }
    public void EnableDragMoveMode()
    {
        dragModeEnabled = true;
        tarPos = transform.position + (transform.forward*5f);
    }
    public void DisableDragMoveMode()
    {
        dragModeEnabled = false;
    }
    public void ResetIt()
    {
        DisableDragMoveMode();
    }

    public void InitialTasks()
    {
        //firefightingManager = LevelValuesAssignManager.instance.firefightinngAreaInLevels[GameManagementMain.instance.levelIndex]; 
        firefightingManager = LevelManager.instance.firefightingManagerInThisLevel;
    }
}
