﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoystickController : MonoBehaviour
{
    public GameObject joystickHolder;
    public GameObject knob;
    public Vector3 pos;
    bool joystickEnabled;
    float sensitivity = 1f;
    public float mag;
    float magnitudeMax = 0.1f;
    public Vector2 output;
    public float ang;
    // Start is called before the first frame update
    void Start()
    {
        JoystickDisable();
        InputMouseSystem.instance.actionTouchIn += JoystickEnable;
        InputMouseSystem.instance.actionTouchOut += JoystickDisable;
    }

    // Update is called once per frame
    void Update()
    {
        if (!joystickEnabled)
        {
            return;
        }
        
        pos = new Vector3(InputMouseSystem.instance.mouseProgressionWRTScreen.x * Screen.width, InputMouseSystem.instance.mouseProgressionWRTScreen.y * Screen.width, 0)*sensitivity;
        mag = pos.magnitude;
        float s = Mathf.Clamp( mag/(magnitudeMax*Screen.width) , 1, Mathf.Infinity);
        pos = pos / s;
        knob.transform.localPosition = pos;
        output = new Vector2(pos.x, pos.y) / (magnitudeMax * Screen.width);
        ang = Mathf.Atan2(output.x, output.y)*Mathf.Rad2Deg;
        //
    }
    public void JoystickEnable()
    {
        if(PlayerControlSystemDefiner.instance.playerRunMode != PlayerRunMode.DragControlled)
        {
            return;
        }
        joystickHolder.SetActive(true);
        joystickEnabled = true;
        joystickHolder.transform.position = Input.mousePosition;
    }
    public void JoystickDisable()
    {
        joystickEnabled = false;
        joystickHolder.SetActive(false);
        knob.transform.localPosition = Vector3.zero;
        output = Vector3.zero;
    }
}
