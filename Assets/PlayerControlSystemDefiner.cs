﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControlSystemDefiner : MonoBehaviour
{
    public PlayerRunMode playerRunMode;
    PlayerPathFollowSystem  playerPathFollowSystem   ;
    PlayerDragMoveSystem    playerDragMoveSystem     ;
    PlayerRightLeftMovement playerRightLeftMovement   ;
    PlayerHealth playerHealth;
    public static PlayerControlSystemDefiner instance;
    public PlayerWaterTank playerWaterTank;
    Vector3 initialPos;
    public ParticleSystem confettiParticle;
    
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        playerPathFollowSystem   = GetComponent<PlayerPathFollowSystem >();
        playerDragMoveSystem     = GetComponent<PlayerDragMoveSystem   >();
        playerRightLeftMovement = GetComponent <PlayerRightLeftMovement>();
        playerHealth = PlayerHealth.instance;

        initialPos = transform.position;

        ChangePlayerRunModeTo(PlayerRunMode.Stationary);
    }

    // Update is called once per frame
    //void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.R))
    //    {
    //        ResetPlayer();
    //    }
    //}
    public void ChangePlayerRunModeTo(PlayerRunMode playerRunMod)
    {
        playerRunMode = playerRunMod;
        switch(playerRunMode){
            case PlayerRunMode.Stationary:
                {
                    playerPathFollowSystem.DisablePathFollowMode();
                    playerDragMoveSystem.DisableDragMoveMode();
                    playerRightLeftMovement.EnableCenterSnapMode();
                    break;
                }
            case PlayerRunMode.PathFollowing:
                {
                    playerPathFollowSystem.EnablePathFollowMode();
                    playerDragMoveSystem.DisableDragMoveMode();
                    //playerRightLeftMovement.EnableCenterSnapMode();
                    break;
                }
            case PlayerRunMode.DragControlled:
                {
                    playerPathFollowSystem.DisablePathFollowMode();
                    playerDragMoveSystem.EnableDragMoveMode();
                    //playerWaterTank.ChangeWaterTankModeTo(WaterTankMode.SprayWater);
                    //playerRightLeftMovement.EnableCenterSnapMode();
                    break;
                }
        }
    }
    //public void StartButtonFun()
    //{
    //    PlayerControlSystemDefiner.instance.ChangePlayerRunModeTo(PlayerRunMode.PathFollowing);
    //    CamFollow.instance.ChangeCameraModeTo(CameraMode.BackClose);
    //}
    //public void RestartButtonFun()
    //{
    //    Application.LoadLevel(0);
    //}
    //
    private void OnTriggerEnter(Collider other)
    {
        if (playerHealth.isDead)
        {
            return;
        }
        if (other.CompareTag("CenterModeTrigger"))
        {
            playerRightLeftMovement.EnableCenterSnapMode();
        }
        if (other.CompareTag("CenterModeDisableTrigger"))
        {
            playerRightLeftMovement.DisableCenterSnapMode();
        }
        if (other.CompareTag("DragModeTrigger"))
        {
            if (GetComponent<PlayerDragMoveSystem>().leftRightFirefight)
            {
                GetComponent<PlayerPathFollowSystem>().BrakeMultiplierTo(0.33f, 0.25f);
                
            }
            else
            {
                ChangePlayerRunModeTo(PlayerRunMode.DragControlled);
            }
            playerWaterTank.ChangeWaterTankModeTo(WaterTankMode.SprayWater);
            CamFollow.instance.ChangeCameraModeTo(CameraMode.TopDown);

            TutorialManager.instance.FlyOverFireTextEnable();
        }
        if (other.CompareTag("CollectWaterTrigger"))
        {
            playerWaterTank.ChangeWaterTankModeTo(WaterTankMode.CollectWater);
        }
        if(other.gameObject.layer == 10)
        {
            PlayerPathFollowSystem.instance.StartCoroutine(PlayerPathFollowSystem.instance.BrakeMulTurnInto(0, 0.33f));
            GameStatesControl.instance.ChangeGameStateTo(GameStates.LevelResult, 2);
            confettiParticle.Play();
        }

    }
    public void ResetPlayer()
    {
        playerRightLeftMovement.ResetIt();
        playerDragMoveSystem.ResetIt();
        playerPathFollowSystem.ResetIt();
        transform.position = initialPos;
        transform.rotation = Quaternion.Euler(Vector3.zero);
        playerHealth.ResetIt();
        playerWaterTank.ResetIt();
        ChangePlayerRunModeTo(PlayerRunMode.Stationary);

        

    }
    public void InitialTasksPlayer()
    {
        //firefightingManager = LevelValuesAssignManager.instance.firefightinngAreaInLevels[GameManagementMain.instance.levelIndex]; 
        playerDragMoveSystem.InitialTasks();
        PlayerPathFollowSystem.instance.PathFollowAssignTarget(LevelValuesAssignManager.instance.pathMakerInLevels[GameManagementMain.instance.levelIndex].pathPoints[0]);
        PlayerControlSystemDefiner.instance.ChangePlayerRunModeTo(PlayerRunMode.PathFollowing);
        playerWaterTank.InitialTasks();
    }
}
