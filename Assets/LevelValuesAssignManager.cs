﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelValuesAssignManager : MonoBehaviour
{
    public static LevelValuesAssignManager instance;
    //public PathStationsInLevel[] pathOnLevel;
    public PathMaker[] pathMakerInLevels;
    public FirefightingManager[] firefightinngAreaInLevels;
    //public PathStation[] pathStationAfterFirefightingInLevels;
    [System.Serializable]
    public class PathStationsInLevel
    {
        public string title;
        public PathStation[] pathStations;
    }
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < firefightinngAreaInLevels.Length; i++)
        {
            firefightinngAreaInLevels[i].AssignPathStationAfterFirefight(i);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
