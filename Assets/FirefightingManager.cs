﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirefightingManager : MonoBehaviour
{
    public List<FlameObj> flameObjsInside;
    public List<FlameObj> flameObjsInCurrent;
    //public static FirefightingManager firefightingManagerInThisLevel;
    public PathStation pathStationAfterFirefight;
    public GameObject MaxX;
    public GameObject MinX;
    public GameObject MaxZ;
    public GameObject MinZ;
    private void Awake()
    {
        //firefightingManagerInThisLevel = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        flameObjsInCurrent.Clear();
        //List<FlameObj> flameObjsTemp = flameObjsInside;
        flameObjsInCurrent.AddRange(flameObjsInside);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void FirefightingSuccessful()
    {
        PlayerPathFollowSystem.instance.PathFollowAssignTarget(pathStationAfterFirefight.pathPointsCreated[0] );
        PlayerControlSystemDefiner.instance.ChangePlayerRunModeTo(PlayerRunMode.PathFollowing);
        CamFollow.instance.ChangeCameraModeTo(CameraMode.BackClose);
        PlayerWaterTank.instance.StopWaterFall();
        PlayerWaterTank.instance.ChangeWaterTankModeTo(WaterTankMode.Stationary);

        LevelManager.instance.SetLevelResultTo(LevelResultType.Success);

        TutorialManager.instance.FlyOverFireTextDisable();
    }
    public void ResetFirefightingManager()
    {
        flameObjsInCurrent.Clear();
        //List<FlameObj> flameObjsTemp = flameObjsInside;
        flameObjsInCurrent.AddRange(flameObjsInside);
        for (int i = 0; i < flameObjsInCurrent.Count; i++)
        {
            flameObjsInCurrent[i].ResetIt();
        }
    }
    public void AssignPathStationAfterFirefight(int level)
    {
        pathStationAfterFirefight = LevelValuesAssignManager.instance.pathMakerInLevels[level].pathStations
            [LevelValuesAssignManager.instance.pathMakerInLevels[level].pathStations.Length - 4];
    }
}
