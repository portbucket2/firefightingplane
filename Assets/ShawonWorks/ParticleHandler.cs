using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleHandler : MonoBehaviour

{
    public ParticleSystem scatter;
    public ParticleSystem fall;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            scatter.Play();
            fall.Stop();
        }
        if (Input.GetKeyDown(KeyCode.V))
        {
            scatter.Stop();
            fall.Play();
        }
    }

}
