﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggersParent : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        foreach (var item in GetComponentsInChildren<MeshRenderer>())
        {
            item.enabled = false;
        }
    }

    // Update is called once per frame
    //void Update()
    //{
    //    
    //}
}
